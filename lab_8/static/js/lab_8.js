  // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '148575649098246',
	  cookie     : true,
	  xfbml      : true,
	  version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
	FB.getLoginStatus(function(response) {
       var loginFlag = false;
	   if (response.status === 'connected') {
		   loginFlag = true;
	   }
	   console.log(response)
	   render(loginFlag);
    })
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        console.log(user)
		html = '<div class="">';
		$('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
			'<div class="container">' +
				'<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
				'<div class="data">' +
				  '<h1>' + user.name + '</h1>' +
				  '<h2>' + user.about + '</h2>' +
				  '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
				'</div>' +
			'</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
		  console.log(feed)
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
			html = '<div class="container">'
            if (value.message && value.story) {
				html += '<div class="feed">' +
						  '<p>' + value.message + '</p>' +
						  '<p>' + value.story + '</p>' +
						'</div>';
            } else if (value.message) {
				html += '<div class="feed">' +
						  '<p>' + value.message + '</p>' +
						'</div>';
            } else if (value.story) {
				html += '<div class="feed">' +
						  '<p>' + value.story + '</p>' +
						'</div>';
            }
			html += '</div>';
			$('#lab8').append(html);
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
	}
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
	 FB.login(function(response){
		console.log(response)
		if(response.status === 'connected') {
			FB.api('/me', function(response) {
				alert('Good to see you, ' + response.name + '.');
			});
			render(true);
		} else {
			alert('User cancelled login or did not fully authorize.');
		}
	 }, {scope:'public_profile,user_posts,publish_actions'});
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
	FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
		  render(false);
        }
     });
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
    FB.api('/me?fields=name,about,email,gender,cover,picture', 'GET', function (response){
      fun(response);
    });
  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
	FB.api('/me/feed?fields=from,link,message,story,created_time', 'GET', function (response){
      fun(response);
    });
  };

  const postFeed = message => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    
	if (message !== "") {
		console.log(message);
		FB.api('/me/feed', 'POST', {message:message});
	}
	else {
		alert('Pesan tidak boleh kosong!');
	}
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };
