from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, friend_list
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

import json

# Create your tests here.
class Lab7UnitTest(TestCase):

	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)
		
	def test_lab_7_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)
		
	def test_lab_7_get_friend_list_json_response(self):
		response = Client().get('/lab-7/get-friend-list/')
		friends = [obj.as_dict() for obj in Friend.objects.all()]
		self.assertJSONEqual(
			str(response.content, encoding='utf8'),
			{'results': friends})
		
	def test_lab_7_post_add_new_friend_and_delete_friend(self):
		response = Client().post('/lab-7/get-mahasiswa-list/1/')
		dict = json.loads(response.content.decode('utf-8'))['mahasiswa_list'][0]
		name = dict['nama']
		npm = dict['npm']
		#Add friend
		response = Client().post('/lab-7/add-friend/', {'name': name, 'npm': npm})
		self.assertEqual(response.status_code, 200)
		#Add friend with same npm
		response = Client().post('/lab-7/add-friend/', {'name': name, 'npm': npm})
		self.assertEqual(response.status_code, 400)
		#Delete friend
		response = Client().post('/lab-7/delete-friend/0/')
		self.assertEqual(response.status_code, 200)
		
	def test_lab_7_post_validate_npm(self):
		friend = Friend.objects.create(friend_name='anonymous', npm='99999999')
		response = Client().post('/lab-7/validate-npm/', {'npm': friend.npm})
		dict = json.loads(response.content.decode('utf-8'))
		self.assertTrue(dict['is_taken'])
		
	def test_lab_7_using_csui_helper_with_wrong_password(self):
		try:
			csui_helper = CSUIhelper('anonim', 'test')
		except Exception as e:
			self.assertIn('username atau password sso salah', str(e))
			
	def test_models_can_create_new_friend(self):
		#Creating a new friend
		name = 'anonymous'
		npm = '9999999999'
		friend = Friend.objects.create(friend_name=name, npm=npm)
		#Dictionary representation
		friend_dict = friend.as_dict()
		self.assertEqual(name, friend_dict['friend_name'])
		self.assertEqual(npm, friend_dict['npm'])
		
	